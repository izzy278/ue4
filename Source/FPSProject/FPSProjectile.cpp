// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSProject.h"
#include "FPSProjectile.h"

// Sets default values
AFPSProjectile::AFPSProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Use a sphere as a simple collision representation.
	CollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	// Set the sphere's collision radius.
	CollisionComponent->InitSphereRadius(15.0f);
	CollisionComponent->BodyInstance.SetCollisionProfileName(TEXT("Projectile")); //use collision channel that we created
	CollisionComponent->OnComponentHit.AddDynamic(this, &AFPSProjectile::OnHit);
	// Set the root component to be the collision component.
	RootComponent = CollisionComponent;

	// Use this component to drive this projectile's movement.
	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
	ProjectileMovementComponent->SetUpdatedComponent(CollisionComponent);
	ProjectileMovementComponent->InitialSpeed = 16000.0f;
	ProjectileMovementComponent->MaxSpeed = 0.0f;
	ProjectileMovementComponent->bRotationFollowsVelocity = true;
	ProjectileMovementComponent->bShouldBounce = false;
	ProjectileMovementComponent->Bounciness = 0.3f;

	//Create
	staticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet"));
	staticMesh->AttachToComponent(CollisionComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	staticMesh->SetMobility(EComponentMobility::Movable);

	SplatMaterial = CreateDefaultSubobject<UMaterial>(TEXT("Splat Material"));

	InitialLifeSpan = 4.0f;
}

// Called when the game starts or when spawned
void AFPSProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFPSProjectile::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Function that initializes the projectile's velocity in the shoot direction.
void AFPSProjectile::FireInDirection(const FVector& ShootDirection)
{
	ProjectileMovementComponent->Velocity = ShootDirection * ProjectileMovementComponent->InitialSpeed;
}

// Function that is called when the projectile hits something.
void AFPSProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	/*if (OtherActor->GetClass() != this->GetClass() && OtherComponent->IsSimulatingPhysics())
	{
		//GEngine->AddOnScreenDebugMessage(-1, 10.0f, FColor::Red, TEXT("HIT"));
		OtherComponent->AddImpulseAtLocation(ProjectileMovementComponent->Velocity * 100.0f, Hit.ImpactPoint);
		FRotator rotVector = Hit.ImpactNormal.Rotation();
		this->Destroy();
	}
	else if (OtherActor->GetClass() != this->GetClass() && !OtherComponent->IsSimulatingPhysics()) {
		if (SplatMaterial) {
			//GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Cyan, TEXT("Found Splat Material"));
			FRotator rotVector = Hit.ImpactNormal.Rotation();
			UGameplayStatics::SpawnDecalAtLocation(OtherActor, SplatMaterial, FVector(50.f, 50.f, 50.f), HitComp->GetComponentLocation(), rotVector);
		}
	}*/
	this->Destroy();
}